# SoftwareEngineeringGroupProject

                Group Project for Software Engineering Mo We 6-7:20 pm

                By: Carissa Barrett, Derek Rogers, (add your name here)

======================================================================================
$$                  Step 1 : figuring out a Project to decide on                    $$
======================================================================================
                            =-=-=-=-=-=-==-=-=-=-=-=-
                                      Ideas
                            =-=-=-=-=-=-==-=-=-=-=-=-

1. restaurant - server or hostess software
2. 

                            =-=-=-=-=-=-==-=-=-=-=-=-
                                    Discussion
                            =-=-=-=-=-=-==-=-=-=-=-=-

Derek:
    restaurant - boring and hard to spell
Carissa:
    restaurant - 

======================================================================================
$$                    Step 2 : Setting Up Everyones Enviornment                     $$
======================================================================================

Unless someone disagrees both Carissa and Derek agree using a MERN stack is what we
should use.

                =-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-
                        Why we think MERN is good (Optional)
                =-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-

MERN has an intuitive database design, I have experience with javascript
callback functions which is used heavily in the node.js, and is a recommendation from
someone who has many years of industry experience.

                =-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-
                        How does one setup a MERN stack?
                =-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-

I would recommend using the 2 resources listed below to most effeciently setup up 
a MERN stack.

1. Follow along from 0:00 to 5:52 of this video:
https://www.youtube.com/watch?v=rpJO0T08Bnc
This video explains how to:
        a. Install Nodejs and mongodb
        b. Verify they are setup correcly
        c. Understand where the package.json comes from

2. My written guide on how to setup:
https://gitlab.com/derogers/softwareengineeringgroupproject/blob/master/HowToSetUpEnviornment.txt



//TODO: If you make it this far, you take over documenting how to setup a MERN stack please


                                        -=-=-=-=-=-=
                                          IF STUCK
                                        -=-=-=-=-=-=

If you are having problems, text Derek Rogers @ 972-816-5695 and I will help you with
whatever problems you are running into. This stuff is hard, don't be afraid to ask for
help.